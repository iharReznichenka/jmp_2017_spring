package com.epam.reznichenka.architecture.mailstorage

import com.epam.reznichenka.architecture.mailstorage.factory.GmailFactory
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.model.Label
import com.google.api.services.gmail.model.ListLabelsResponse
import com.google.api.services.gmail.model.Message
import spock.lang.Shared
import spock.lang.Specification

class GmailApiIntegrationSpec extends Specification {

    @Shared
    Gmail gmail
    def meUserId = "me"

    def setupSpec() {
        gmail = GmailFactory.getOrCreate()
    }

    def "gmail api is working"() {
        when: "Print the labels in the meUserId's account"

        ListLabelsResponse listResponse =
                gmail.users().labels().list(meUserId).execute()
        List<Label> labels = listResponse.getLabels()
        labels.empty ? println("No labels found") : labels.each { println "- ${it.name}" }

        then:
        assert !labels.empty
        noExceptionThrown()
    }

    def "gets the list of mails"() {
        when:
        def requestList = gmail.users().messages().list(meUserId).with {
            setMaxResults(20L)
            setIncludeSpamTrash(false)
        }
        def response = requestList.execute()
        def messages = response.getMessages()
        messages.each { println it.getId() }

        then:
        noExceptionThrown()
    }

    def "gets message for userId by messageId with snippet"() {
        when:
        def requestList = gmail.users().messages().list(meUserId).with {
            setMaxResults(5L)
            setIncludeSpamTrash(false)
        }
        def messages = requestList.execute()
        messages.getMessages().each {
            Message message = gmail.users().messages().get(meUserId, it.getId()).execute()
            println message.getSnippet()
        }
        then:
        noExceptionThrown()
    }
}