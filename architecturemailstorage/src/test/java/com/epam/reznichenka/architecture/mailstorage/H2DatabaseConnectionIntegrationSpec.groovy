package com.epam.reznichenka.architecture.mailstorage

import spock.lang.Specification

import java.sql.DriverManager

class H2DatabaseConnectionIntegrationSpec extends Specification {

    def "verifies that in memory H2 database is working"() {
        when:
        def connection = DriverManager.getConnection("jdbc:h2:mem:~/mailstorage", "sa", "sa")
        println connection.metaData

        then:
        noExceptionThrown()
    }
}