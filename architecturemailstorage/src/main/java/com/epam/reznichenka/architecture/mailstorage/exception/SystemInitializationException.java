package com.epam.reznichenka.architecture.mailstorage.exception;

/**
 * Should be thrown when system could not be properly instantiated and there is no reason to continue execution
 */
public class SystemInitializationException extends RuntimeException {

	public SystemInitializationException(String message) {
		super(message);
	}

	public SystemInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public SystemInitializationException(Throwable cause) {
		super(cause);
	}
}
