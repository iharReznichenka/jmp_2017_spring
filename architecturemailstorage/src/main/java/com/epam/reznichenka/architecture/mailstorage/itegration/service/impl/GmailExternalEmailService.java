package com.epam.reznichenka.architecture.mailstorage.itegration.service.impl;

import com.epam.reznichenka.architecture.mailstorage.entity.Email;
import com.epam.reznichenka.architecture.mailstorage.factory.GmailFactory;
import com.epam.reznichenka.architecture.mailstorage.itegration.service.ExternalEmailService;
import com.epam.reznichenka.architecture.mailstorage.mappers.GmailEmailMapper;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import lombok.Synchronized;

import java.io.IOException;
import java.util.*;

/**
 * Provides ability to work Gmail as a Service
 */
public class GmailExternalEmailService implements ExternalEmailService {

	private static GmailExternalEmailService INSTANCE;

	private static final Gmail gmail = GmailFactory.getOrCreate();
	private String ME_USER_ID = "me";

	@Override
	public Collection<Email> fetchLast(long count) {
		List<Message> lastMetaMessages = getLastMetaMessages(count);
		Collection<Email> emails = new ArrayList<>();
		for (Message metaMessage : lastMetaMessages) {
			Optional<Message> message = fetchEmail(metaMessage.getId());
			if (message.isPresent()) {
				Optional<Email> email = GmailEmailMapper.getInstance().toEmail(message.get());
				email.ifPresent(emails::add);
			}
		}
		return emails;
	}

	private Optional<Message> fetchEmail(String emailId) {
		try {
			Gmail.Users.Messages.Get request = gmail.users().messages().get(ME_USER_ID, emailId);
			return Optional.ofNullable(request.execute());
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	private List<Message> getLastMetaMessages(long count) {
		ListMessagesResponse response;
		try {
			response = gmail.users().messages().list(ME_USER_ID).setMaxResults(5L).execute();
		} catch (IOException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
		return response.getMessages();
	}

	@Synchronized
	public static GmailExternalEmailService getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GmailExternalEmailService();
		}
		return INSTANCE;
	}
}
