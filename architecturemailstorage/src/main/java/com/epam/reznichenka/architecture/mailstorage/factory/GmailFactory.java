package com.epam.reznichenka.architecture.mailstorage.factory;

import com.epam.reznichenka.architecture.mailstorage.exception.SystemInitializationException;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import lombok.Synchronized;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Code is taken from Gmail Api Quickstart - https://developers.google.com/gmail/api/quickstart/java
 */
public class GmailFactory {

	private GmailFactory() {
	}

	private static Gmail gmail;

	private static final String APPLICATION_NAME = "JMP Mail Storage";
	private static FileDataStoreFactory DATA_STORE_FACTORY;
	private static final JsonFactory JSON_FACTORY =
			  JacksonFactory.getDefaultInstance();
	private static HttpTransport HTTP_TRANSPORT;
	/**
	 * Directory to store user credentials for this application.
	 */
	private static final File DATA_STORE_DIR = new File(
			  System.getProperty("user.home"),
			  ".credentials/jmp-architecture-mail-storage");

	/**
	 * Global instance of the scopes required by this quickstart.
	 */
	private static final List<String> SCOPES = Arrays.asList(
			  GmailScopes.MAIL_GOOGLE_COM,
			  GmailScopes.GMAIL_READONLY);

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates an authorized Credential object.
	 *
	 * @return an authorized Credential object.
	 * @throws IOException
	 */
	private static Credential authorize() throws IOException {
		// Load client secrets.
		InputStream inputStream = GmailFactory.class.getResourceAsStream("/client_secret.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(inputStream));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
				  .setDataStoreFactory(DATA_STORE_FACTORY)
				  .setAccessType("offline")
				  .build();
		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
				  .authorize("user");
		System.out.println("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
		return credential;
	}

	/**
	 * Build and return an authorized Gmail client factory.
	 *
	 * @return an authorized Gmail client factory
	 * @throws SystemInitializationException when could not make instance
	 */
	@Synchronized
	public static Gmail getOrCreate() throws SystemInitializationException {
		if (gmail == null) {
			Credential credential;
			try {
				credential = authorize();
			} catch (IOException ex) {
				throw new SystemInitializationException("Could not instantiate Gmail integration", ex);
			}
			gmail = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
					  .setApplicationName(APPLICATION_NAME)
					  .build();
		}
		return gmail;
	}

}
