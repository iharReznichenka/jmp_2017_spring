package com.epam.reznichenka.architecture.mailstorage;

import com.epam.reznichenka.architecture.mailstorage.entity.Email;
import com.epam.reznichenka.architecture.mailstorage.factory.GmailFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.*;
import com.iciql.Db;
import lombok.Cleanup;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Implements all the functionality in one place.
 * Implementation intentionally violates DRY, KISS, SOLID principles.
 */
public class ViolatedImplementation {

	/*
	* Violations examples:
	* SRP - API based operations mixed with persistence layer
	* OCP - there is nothing to extend. the only thing we can do is to rewrite entire run method
	* LSP - no abstractions, interfaces, so no possibility to be agile
	*
	*/
	public void run() {
		try {
			Gmail gmail = GmailFactory.getOrCreate();
			ListMessagesResponse response = gmail.users().messages().list("me").setMaxResults(5L).execute();
			List<Message> messages = response.getMessages();
			@Cleanup Db db = Db.open("jdbc:h2:mem:~mailstorage", "sa", "sa");
			for (Message message : messages) {
				Message messageWithPayload = gmail.users().messages().get("me", message.getId()).execute();
				Email email = convertToEntity(messageWithPayload);
				db.insert(email);
			}

			// Let's check persistence state
			Email email = new Email();
			List<String> subjects = db.from(email).selectDistinct(email.getSubject());
			subjects.forEach(System.out::println);
//			db.executeQuery(Email.class, "SELECT * FROM EMAILS");
			db.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/* Violates DRY. Should be moved to the separate mappers/factory */
	private Email convertToEntity(Message message) {
		Email email = new Email();
		email.setEmailId(message.getId());

		MessagePart payload = message.getPayload();
		if (payload == null) {
			return null;
		}
		Optional<MessagePartBody> body = Optional.ofNullable(payload.getBody());
		if (body.isPresent()) {
			email.setBody(body.get().getData());
		} else {
			email.setBody(StringUtils.EMPTY);
		}

		List<MessagePartHeader> headers = payload.getHeaders();
		Optional<MessagePartHeader> subject = findHeader(headers, "Subject");
		subject.ifPresent(it -> email.setSubject(it.getValue()));

		Optional<MessagePartHeader> from = findHeader(headers, "From");
		from.ifPresent(it -> email.setFrom(it.getValue()));

		Optional<MessagePartHeader> to = findHeader(headers, "To");
		to.ifPresent(it -> email.setTo(it.getValue()));
		return email;
	}

	private Optional<MessagePartHeader> findHeader(Collection<MessagePartHeader> headers, String headerName) {
		if (StringUtils.isBlank(headerName)) {
			return Optional.empty();
		}
		return headers.stream().filter(it -> headerName.equalsIgnoreCase(it.getName())).findFirst();
	}
}
