package com.epam.reznichenka.architecture.mailstorage.persistance.service.impl;

import com.epam.reznichenka.architecture.mailstorage.util.IciqlUtils;
import com.epam.reznichenka.architecture.mailstorage.entity.Email;
import com.epam.reznichenka.architecture.mailstorage.persistance.service.EmailDao;
import com.iciql.Db;
import lombok.Cleanup;
import lombok.Synchronized;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Implements {@link Email} persistence level using Iciql lib
 */
public class IciqlEmailDao implements EmailDao {

	private static IciqlEmailDao INSTANCE;
	private static final Email TEMPLATE = new Email();

	@Override
	public Collection<Email> findAll() {
		return IciqlUtils.getDb().from(TEMPLATE).select();
	}

	@Override
	public Optional<Email> findById(final String emailId) {
		Email result = IciqlUtils.getDb().from(TEMPLATE).selectFirst();
		return Optional.of(result);
	}

	@Override
	public void insert(Collection<Email> emails) {
		@Cleanup Db db = IciqlUtils.getDb();
		db.insertAll(new ArrayList<>(emails));
	}

	@Override
	public void update(Collection<Email> emails) {
		@Cleanup Db db = IciqlUtils.getDb();
		db.updateAll(new ArrayList<>(emails));
	}

	@Synchronized
	public static IciqlEmailDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new IciqlEmailDao();
		}
		return INSTANCE;
	}
}
