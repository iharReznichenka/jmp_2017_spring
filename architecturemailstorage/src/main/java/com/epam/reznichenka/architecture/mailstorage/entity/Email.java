package com.epam.reznichenka.architecture.mailstorage.entity;

import com.iciql.Iciql;

/**
 * POJO-like entity to store email data
 */
@Iciql.IQTable(primaryKey = "id")
public class Email {
	@Iciql.IQColumn(primaryKey = true, autoIncrement = true)
	private Integer id;
	@Iciql.IQColumn(length = 50, trim = true)
	private String emailId;
	@Iciql.IQColumn(length = 200, trim = true)
	private String subject;
	@Iciql.IQColumn(name = "email_from", length = 50, trim = true)
	private String from;
	@Iciql.IQColumn(name = "email_to", length = 50, trim = true)
	private String to;
	@Iciql.IQColumn(length = 5000, trim = true)
	private String body;

	public Email() {
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "Email{" +
				  "id=" + id +
				  ", emailId='" + emailId + '\'' +
				  ", subject='" + subject + '\'' +
				  ", from='" + from + '\'' +
				  ", to='" + to + '\'' +
				  ", body='" + body + '\'' +
				  '}';
	}
}
