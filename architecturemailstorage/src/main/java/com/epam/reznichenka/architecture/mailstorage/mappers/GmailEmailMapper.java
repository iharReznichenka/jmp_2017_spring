package com.epam.reznichenka.architecture.mailstorage.mappers;

import com.epam.reznichenka.architecture.mailstorage.entity.Email;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;
import lombok.Synchronized;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class GmailEmailMapper {

	private static GmailEmailMapper INSTANCE;

	public Optional<Email> toEmail(final Message message) {
		Email email = new Email();
		email.setEmailId(message.getId());

		MessagePart payload = message.getPayload();
		if (payload == null) {
			return Optional.empty();
		}
		Optional<MessagePartBody> body = Optional.ofNullable(payload.getBody());
		if (body.isPresent()) {
			email.setBody(body.get().getData());
		} else {
			email.setBody(StringUtils.EMPTY);
		}

		List<MessagePartHeader> headers = payload.getHeaders();
		Optional<MessagePartHeader> subject = findHeader(headers, "Subject");
		subject.ifPresent(it -> email.setSubject(it.getValue()));

		Optional<MessagePartHeader> from = findHeader(headers, "From");
		from.ifPresent(it -> email.setFrom(it.getValue()));

		Optional<MessagePartHeader> to = findHeader(headers, "To");
		to.ifPresent(it -> email.setTo(it.getValue()));
		return Optional.of(email);
	}

	private Optional<MessagePartHeader> findHeader(Collection<MessagePartHeader> headers, String headerName) {
		if (StringUtils.isBlank(headerName)) {
			return Optional.empty();
		}
		return headers.stream().filter(it -> headerName.equalsIgnoreCase(it.getName())).findFirst();
	}

	@Synchronized
	public static GmailEmailMapper getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GmailEmailMapper();
		}
		return INSTANCE;
	}
}
