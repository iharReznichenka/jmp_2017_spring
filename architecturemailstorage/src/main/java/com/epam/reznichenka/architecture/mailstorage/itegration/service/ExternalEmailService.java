package com.epam.reznichenka.architecture.mailstorage.itegration.service;

import com.epam.reznichenka.architecture.mailstorage.entity.Email;

import java.util.Collection;

/**
 * Provides ability to get {@link com.epam.reznichenka.architecture.mailstorage.entity.Email} from external mail service
 */
public interface ExternalEmailService {

	/**
	 * @param count of emails to get last by recieved date
	 * @return collection fetched emails
	 */
	Collection<Email> fetchLast(long count);

}
