package com.epam.reznichenka.architecture.mailstorage;

import com.epam.reznichenka.architecture.mailstorage.entity.Email;
import com.epam.reznichenka.architecture.mailstorage.exception.SystemInitializationException;
import com.epam.reznichenka.architecture.mailstorage.itegration.service.ExternalEmailService;
import com.epam.reznichenka.architecture.mailstorage.itegration.service.impl.GmailExternalEmailService;
import com.epam.reznichenka.architecture.mailstorage.persistance.service.EmailDao;
import com.epam.reznichenka.architecture.mailstorage.persistance.service.impl.IciqlEmailDao;
import com.epam.reznichenka.architecture.mailstorage.util.IciqlUtils;
import com.iciql.Db;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Entry point
 */
public class Application {

	private Application() {
	}

	public static void main(String[] args) {
		try {
			ExternalEmailService externalEmailService = GmailExternalEmailService.getInstance();
			Collection<Email> emails = externalEmailService.fetchLast(10L);
			emails.forEach(System.out::println);
			EmailDao emailDao = IciqlEmailDao.getInstance();
			emailDao.insert(emails);
			emailDao.findAll().forEach(System.out::println);
		} catch (SystemInitializationException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}
}
