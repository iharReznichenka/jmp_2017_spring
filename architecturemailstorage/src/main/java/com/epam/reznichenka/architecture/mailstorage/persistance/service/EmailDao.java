package com.epam.reznichenka.architecture.mailstorage.persistance.service;

import com.epam.reznichenka.architecture.mailstorage.entity.Email;

import java.util.Collection;
import java.util.Optional;

/**
 * Provides ability to work {@link com.epam.reznichenka.architecture.mailstorage.entity.Email} on persistence level
 */
public interface EmailDao {
	Collection<Email> findAll();
	Optional<Email> findById(String emailId);
	void insert(Collection<Email> emails);
	void update(Collection<Email> emails);
}
