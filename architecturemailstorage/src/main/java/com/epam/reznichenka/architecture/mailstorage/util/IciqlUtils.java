package com.epam.reznichenka.architecture.mailstorage.util;

import com.iciql.Db;
import lombok.Synchronized;

/**
 * Probably should be refactored to avoid usage of util classes at all
 */
public class IciqlUtils {

	private static Db DB_INSTANCE = createDbInstance();

	public static Db getDb() {
		return DB_INSTANCE;
	}

	@Synchronized
	private static Db createDbInstance() {
		return Db.open("jdbc:h2:mem:~mailstorage", "sa", "sa"); // TODO: move DB vars to the properties
	}
}
