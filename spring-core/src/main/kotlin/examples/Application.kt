@file:JvmName(name = "Application")

package examples

import examples.properties.DemoProperties

fun main(args: Array<String>) {
//    DemoMethodInjection.main(arrayOf())
//    DemoMethodReplacement.main(arrayOf())
//    DemoAutowiringUsingXmlConfiguration.main(arrayOf())
//    DemoBeanInitialization.main(arrayOf())
//    DemoBeanDestructuring.main(arrayOf())
//    DemoFactoryBeans.main(arrayOf())
//    DemoCustomEditor.main(arrayOf())
//    DemoMessageSource.main(arrayOf())
    DemoProperties.main(arrayOf())
}