package examples.autowiringxml

import org.springframework.context.support.GenericXmlApplicationContext

object DemoAutowiringUsingXmlConfiguration {
    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext()
        ctx.load("classpath:META-INF/spring/context-autowiringxml.xml")
        ctx.refresh()

        var bean: Target?

        println("Using byName:\n")
        bean = ctx.getBean("targetByName") as Target
        println(bean)

        println("\nUsing byType:\n")
        bean = ctx.getBean("targetByType") as Target
        println(bean)

        println("\nUsing constructor:\n")
        bean = ctx.getBean("targetConstructor") as Target
        println(bean)

        println("\nUsing autowire=`default`\n")
        bean = ctx.getBean("targetDefault") as Target
        println(bean)
    }
}