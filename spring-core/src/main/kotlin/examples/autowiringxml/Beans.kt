package examples.autowiringxml

class Target {
    var foo: Foo? = null
        set(value) {
            println("Setting property foo with value $value")
            field = value
        }
    var bar1: Bar? = null
    set(value) {
        println("Setting property bar1 with value $value")
        field = value
    }
    var bar2: Bar? = null
    set(value) {
        println("Setting property bar2 with value $value")
        field = value
    }

    constructor()

    constructor(foo: Foo) {
        println("Using Target(foo)")
    }

    constructor(foo: Foo, bar: Bar) {
        println("Using Target(foo, bar)")
    }
}

class Foo
class Bar