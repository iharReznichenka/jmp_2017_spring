package examples.methodreplacement

open class ReplacementTarget {
    fun formatMessage(msg: String): String {
        return "<h1>$msg</h1>"
    }

    fun formatMessage(msg: Any): String {
        return "<h1>$msg</h1>"
    }
}