package examples.methodreplacement

import org.springframework.beans.factory.support.MethodReplacer
import java.lang.reflect.Method

class FormatMessageReplacer : MethodReplacer {

    override fun reimplement(obj: Any?, method: Method?, args: Array<out Any>?): Any {
        if (isFormatMessageMethod(method!!)) {
            val msg = args!![0] as String
            return "<h3>$msg</h3>"
        } else {
            throw IllegalArgumentException("Unable to reimplement method ${method.name}")
        }
    }

    private fun isFormatMessageMethod(method: Method): Boolean {
        if (method.parameterTypes.size != 1) {
            return false
        }

        if ("formatMessage" != method.name) {
            return false
        }

        if (method.returnType != String::class.java) {
            return false
        }

        if (method.parameterTypes[0] != String::class.java) {
            return false
        }

        return true
    }
}