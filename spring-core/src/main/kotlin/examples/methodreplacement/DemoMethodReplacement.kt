package examples.methodreplacement

import org.springframework.context.support.GenericXmlApplicationContext
import org.springframework.util.StopWatch

// TODO: seems like does not work in kotlin both for 4.3.7 and 5.0M versions. CGLIB bug?
object DemoMethodReplacement {
    @JvmStatic fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext().apply {
            load("classpath:META-INF/spring/context-method-replacement.xml")
            refresh()
        }

        val replacementTarget = ctx.getBean("replaced") as ReplacementTarget
        val standardTarget = ctx.getBean("standard") as ReplacementTarget

        displayInfo(replacementTarget)
        displayInfo(standardTarget)
    }

    private fun displayInfo(target: ReplacementTarget) {
        System.out.println(target.formatMessage("Hello World!"))

        val stopWatch = StopWatch().apply {
            start("performanceTest")
        }

        val triesCount = 999_999
        for (x in 0..triesCount) {
            target.formatMessage("foo")
        }

        stopWatch.stop()
        println("$triesCount invocations took: ${stopWatch.totalTimeMillis} ms.")
    }
}
