package examples.properties

import org.springframework.context.ApplicationContext
import org.springframework.context.support.GenericXmlApplicationContext
import org.springframework.core.env.MapPropertySource

object DemoProperties {
    @JvmStatic
    fun main(args: Array<String>) {
        val context = GenericXmlApplicationContext().apply {
            load("classpath:META-INF/spring/context-properties-and-placeholder.xml")
            refresh()
        }

        demoApplicationProperties(context)
        demoEnvironmentAndMutableProperties(context)

    }

    fun demoApplicationProperties(context: ApplicationContext) {
        val config = context.getBean("config") as Config
        val appHome = config.getProperty("application.home")
        val userHome = config.getProperty("user.home")
        println(appHome)
        println(userHome)
    }

    fun demoEnvironmentAndMutableProperties(context: GenericXmlApplicationContext) {
        val appMap = mapOf("user.home" to "application_home")
        context.environment.propertySources.addFirst(MapPropertySource("CUSTOM_PROPERTIES", appMap))

        println("user.home: ${System.getProperty("user.home")}")
        println("JAVA_HOME: ${System.getenv("JAVA_HOME")}")

        println("user.home: ${context.environment.getProperty("user.home")}")
        println("JAVA_HOME: ${context.environment.getProperty("JAVA_HOME")}")
    }


}
