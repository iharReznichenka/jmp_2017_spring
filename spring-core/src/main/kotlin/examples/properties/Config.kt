package examples.properties

class Config(val properties: Map<String, String>) {
    fun getProperty(qualifier: String): String = properties.getOrDefault(qualifier, "<value is missing>")
}