package examples.methodinjection

interface DemoBean {
    fun getMyHelper(): MyHelper
    fun someOperation()
}

abstract class AbstractLookupDemoBean : DemoBean {
    override abstract fun getMyHelper(): MyHelper

    override fun someOperation() {
        getMyHelper().doSomethingHelpful()
    }
}

class StandardLookupDemoBean(val helper: MyHelper) : DemoBean {

    override fun getMyHelper(): MyHelper {
        return helper
    }

    override fun someOperation() {
        helper.doSomethingHelpful()
    }
}

class MyHelper {
    fun doSomethingHelpful() {
        println("Doing helpful stuff")
    }
}