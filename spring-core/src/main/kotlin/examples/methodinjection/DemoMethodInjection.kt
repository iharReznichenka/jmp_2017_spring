package examples.methodinjection

import org.springframework.context.support.GenericXmlApplicationContext
import org.springframework.util.StopWatch

object DemoMethodInjection {

    @JvmStatic fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext()
        ctx.load("classpath:META-INF/spring/context-method-lookup.xml")
        ctx.refresh()

        val abstractBean = ctx.getBean("abstractLookupBean") as DemoBean
        val standardBean = ctx.getBean("standardLookupBean") as DemoBean

        displayInfo(standardBean)
        displayInfo(abstractBean)
    }

    fun displayInfo(bean: DemoBean) {
        val helper1 = bean.getMyHelper()
        val helper2 = bean.getMyHelper()

        println("Are Helper Instances the Same?: ${helper1 === helper2}")

        val stopWatch = StopWatch().apply {
            start("lookup demo for $bean")
        }

        val triesCount = 50
        for (x in 0..triesCount) {
            bean.getMyHelper().doSomethingHelpful()
        }

        stopWatch.stop()

        println("$triesCount gets took ${stopWatch.totalTimeMillis} ms.")
    }
}