package examples.beaninitialization

import org.springframework.beans.factory.InitializingBean
import javax.annotation.PostConstruct

class Bean : InitializingBean {

    init {
        println("Using primary constructor")
    }

    var foo: Foo? = null
        set(value) {
            println("foo is changed to $value")
            field = value
        }

    var bar: Bar? = null
        set(value) {
            println("bar is changed to $value")
            field = value
        }

    override fun afterPropertiesSet() {
        println("using afterPropertiesSet")
    }

    fun initMethod() {
        println("using xml init-method")
    }

    @PostConstruct
    fun postConstruct() {
        println("using post construct")
    }
}

class Foo
class Bar