package examples.beaninitialization

import org.springframework.context.support.GenericXmlApplicationContext

// priority is: constructor -> setters -> @PostConstruct -> afterPropertiesSet -> init-method
object DemoBeanInitialization {
    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext().apply {
            load("classpath:META-INF/spring/context-beaninitialization.xml")
            refresh()
        }
        val bean = ctx.getBean("bean") as Bean
        println(bean)
    }
}