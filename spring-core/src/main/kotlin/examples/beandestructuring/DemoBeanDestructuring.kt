package examples.beandestructuring

import org.springframework.context.support.GenericXmlApplicationContext


/**
 * call to the ctx.destroy() is required, but it will be difficult to know the exact place to call
 * ctx.registerShutdownHook() is more convenient to use
 * The result order of destructuring is: @PreDestroy, DisposableBean.destroy(), destroy-method
 **/
object DemoBeanDestructuring {
    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext()
        ctx.load("classpath:META-INF/spring/context-beandestructuring.xml")
        ctx.registerShutdownHook()
        ctx.refresh()

        val bean = ctx.getBean("destructiveBean") as Bean
        println(bean)
    }
}