package examples.beandestructuring

import org.springframework.beans.factory.DisposableBean
import javax.annotation.PreDestroy

class Bean : DisposableBean {
    lateinit var foo: Foo
    lateinit var bar: Bar

    fun destroyMethod() {
        println("using destroy method")
    }

    override fun destroy() {
        println("using disposable bean")
    }

    @PreDestroy
    fun preDestroy() {
        println("using @PreDestroy. component scan is required")
    }
}

class Foo
class Bar
