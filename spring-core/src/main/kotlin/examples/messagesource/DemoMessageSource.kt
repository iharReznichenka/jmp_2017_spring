package examples.messagesource

import org.springframework.context.support.GenericXmlApplicationContext
import java.util.*

object DemoMessageSource {
    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext().apply {
            load("classpath:META-INF/spring/context-messagesource.xml")
            refresh()
        }
        val rus = Locale("ru", "RU")
        val englishError = ctx.getMessage("error", null, Locale.ENGLISH)
        val russianWarning = ctx.getMessage("warning", null, rus)
        val greeting = ctx.getMessage("greeting", arrayOf<Any>("Ihar", "Reznichenka"), Locale.ENGLISH)
        println(englishError)
        println(russianWarning)
        println(greeting)
    }
}
