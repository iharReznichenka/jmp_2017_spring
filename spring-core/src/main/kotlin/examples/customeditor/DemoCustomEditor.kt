package examples.customeditor

import org.springframework.context.support.GenericXmlApplicationContext

object DemoCustomEditor {
    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = GenericXmlApplicationContext().apply {
            load("classpath:META-INF/spring/context-custom-editors.xml")
            refresh()
        }

        val bean = ctx.getBean("beanWithName") as BeanWithName
        println(bean)
    }
}