package examples.customeditor

import java.beans.PropertyEditorSupport

class NamePropertyEditor : PropertyEditorSupport() {
    override fun setAsText(text: String?) {
        val values = text!!.split(" ")
        value = Name(values[0], values[1])
    }
}