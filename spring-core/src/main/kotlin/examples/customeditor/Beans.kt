package examples.customeditor

data class BeanWithName(val name: Name)

data class Name(val firstName: String, val lastName: String)