package examples.factorybeans

import org.springframework.context.support.GenericXmlApplicationContext

object DemoFactoryBeans {
    @JvmStatic
    fun main(args: Array<String>) {
        val context = GenericXmlApplicationContext().apply {
            load("classpath:META-INF/spring/context-factory-beans.xml")
            refresh()
        }

        val digester = context.getBean("messageDigester") as MessageDigester
        digester.digest("Hello, World!")
    }
}