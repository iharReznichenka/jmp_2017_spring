package examples.factorybeans

import java.security.MessageDigest

class MessageDigester {

    lateinit var digests: List<MessageDigest>

    fun digest(message: String) {
        digests.forEach { doDigest(message, it) }
    }

    private fun doDigest(msg: String, digest: MessageDigest) {
        println("Using algorithm: ${digest.algorithm}")
        digest.reset()
        val bytes = msg.toByteArray()
        val out = digest.digest(bytes)
        println(out)
    }
}