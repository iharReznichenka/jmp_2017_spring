package examples.factorybeans

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MessageDigestFactory {

    var algorithm: String = "MD5"

    @Throws(NoSuchAlgorithmException::class)
    fun createInstance(): MessageDigest {
        return MessageDigest.getInstance(algorithm)
    }
}