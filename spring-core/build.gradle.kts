buildscript {

    repositories {
        gradleScriptKotlin()
    }

    dependencies {
        classpath(kotlinModule("gradle-plugin"))
    }
}

plugins {
    application
}

apply {
    plugin("kotlin")
}

application {
    mainClassName = "examples.Application"
}

repositories {
    gradleScriptKotlin()
    maven {
        setUrl("https://repo.spring.io/libs-milestone")
    }
}

dependencies {
    compile(kotlinModule("stdlib"))
    compile(group = "org.springframework", name = "spring-context", version = "5.0.0.M5")
}
