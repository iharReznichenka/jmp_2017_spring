package com.epam.reznichenka.jmp.troubleshooting.task2;

import com.epam.reznichenka.jmp.troubleshooting.model.Resource;
import com.epam.reznichenka.jmp.troubleshooting.runnable.ConnectResources;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * Realistic Deadlock with two resources
 */
public class Task2App {

	public void run() {
		ExecutorService executorService = Executors.newFixedThreadPool(8);

		Resource first = new Resource("First");
		Resource second = new Resource("Second");
		ConnectResources firstToSecond = new ConnectResources(first, second);
		ConnectResources secondToFirst = new ConnectResources(second, first);

		int firstToSecondThreadsCount = 3;
		int secondToFirstThreadsCount = 3;

		IntStream.rangeClosed(1, firstToSecondThreadsCount).forEach(it -> executorService.submit(firstToSecond));
		IntStream.rangeClosed(1, secondToFirstThreadsCount).forEach(it -> executorService.submit(secondToFirst));
	}
}
