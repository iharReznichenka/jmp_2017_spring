package com.epam.reznichenka.jmp.troubleshooting.runnable;

import com.epam.reznichenka.jmp.troubleshooting.model.Resource;

public class ConnectResources implements Runnable {

	private Resource from;
	private Resource to;

	public ConnectResources(Resource from, Resource to) {
		this.from = from;
		this.to = to;
	}

	@Override
	public void run() {
		from.connect(to);
	}
}
