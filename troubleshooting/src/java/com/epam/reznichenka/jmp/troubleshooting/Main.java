package com.epam.reznichenka.jmp.troubleshooting;

import com.epam.reznichenka.jmp.troubleshooting.task5.Task5AppWithoutLeak;
import com.epam.reznichenka.jmp.troubleshooting.task5.Task5WithLeak;


/**
 * Entry point
 */
public class Main
{

	private Main()
	{
	}

	public static void main(String[] args)
	{
		//		new Task1App().run();
		//		new Task2App().run();
		//		new Task3App().run();
		//		new Task4App().run();
		new Task5AppWithoutLeak().run();
//		new Task5WithLeak().run();
	}
}
