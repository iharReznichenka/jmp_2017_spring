package com.epam.reznichenka.jmp.troubleshooting.task5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class CommonFunctionality
{
	private static final String DELIMITER = " ";

	public ArrayList<String> readTestData(String filename) throws FileNotFoundException
	{
		File testData = getFileFromResources("task5_data.txt");
		if (testData == null || !testData.exists())
		{
			throw new FileNotFoundException("Can't find the data file.");
		}

		ArrayList<String> words = new ArrayList<>();
		try (FileReader fileReader = new FileReader(testData))
		{
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null)
			{
				String[] lineWords = line.split(DELIMITER);
				words.addAll(Arrays.asList(lineWords));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return words;
	}

	private File getFileFromResources(String filename)
	{
		ClassLoader classLoader = getClass().getClassLoader();
		URL resource = classLoader.getResource(filename);
		return resource != null ? new File(resource.getFile()) : null;
	}


}
