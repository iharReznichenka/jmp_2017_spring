package com.epam.reznichenka.jmp.troubleshooting.task1;

import com.epam.reznichenka.jmp.troubleshooting.model.Resource;
import com.epam.reznichenka.jmp.troubleshooting.runnable.ConnectResources;

/**
 * Simple deadlock with two resources
 */
public class Task1App {

	public void run() {
		Resource first = new Resource("First");
		Resource second = new Resource("Second");
		ConnectResources firstToSecond = new ConnectResources(first, second);
		ConnectResources secondToFirst = new ConnectResources(second, first);
		new Thread(firstToSecond).start();
		new Thread(secondToFirst).start();
	}
}
