package com.epam.reznichenka.jmp.troubleshooting.task5;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


/**
 * Implement a java application that does the following:
 * <p>
 * Reads the text file (from \\EPBYMINSA0000.minsk.epam.com\Training Materials\EPAM Mentoring\Java Mentoring Program\Education
 * Modules\General.TroubleshootingTask #5 - Data.zip) line by line.
 * Splits each line into words and stores them in an ArrayList.
 * Takes the last 3 words from the list (using java.util.List#subList method) and puts this sublist into another list. The result
 * should be a list of 3-word lists (List<List<String>>).
 */
public class Task5WithLeak
{
	public void run()
	{
		CommonFunctionality commonFunctionality = new CommonFunctionality();
		try
		{
			ArrayList<String> words = commonFunctionality.readTestData("task5_data.txt");
			List<List<String>> listsOfThreeWords = convertToListOfThreeWordsLists(words);
			System.out.println("Starting infinity loop to be able to make memory dump");
			while (true)
			{

			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	private List<List<String>> convertToListOfThreeWordsLists(List<String> words)
	{
		List<List<String>> result = new ArrayList<>();
		int cursor = words.size() - 1;
		while (cursor > 2)
		{
			List<String> threeWordsList = words.subList(cursor - 2, cursor);
			result.add(threeWordsList);
			cursor -= 3;
		}
		return result;
	}
}
