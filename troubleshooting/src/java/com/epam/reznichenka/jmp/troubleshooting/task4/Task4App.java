package com.epam.reznichenka.jmp.troubleshooting.task4;

import com.epam.reznichenka.jmp.troubleshooting.model.Bottleneck;

import java.util.stream.IntStream;

/**
 * Reproduce bottleneck
 */
public class Task4App {

	public void run() {
		Bottleneck bottleneck = new Bottleneck();
		int countOfThreads = 4;
		IntStream.rangeClosed(1, countOfThreads).forEach(it -> new Thread(bottleneck::doHardWork).start());
	}
}
