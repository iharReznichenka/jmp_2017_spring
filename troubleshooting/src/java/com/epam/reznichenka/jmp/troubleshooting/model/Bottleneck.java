package com.epam.reznichenka.jmp.troubleshooting.model;

public class Bottleneck {

	public synchronized void doHardWork() {
		String threadName = Thread.currentThread().getName();
		System.out.format("%s start doing hard work%n", threadName);
		try {
			// Mocking long time operation
			Thread.sleep(5000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.format("%s completed%n", threadName);
	}
}
