package com.epam.reznichenka.jmp.troubleshooting.model;

public class Resource {

	private String name;

	public Resource(String name) {
		this.name = name;
	}

	public synchronized void connect(final Resource target) {
		System.out.format("%s waits for answer from %s%n", name, target.getName());
		target.answer(this);
	}

	public synchronized void answer(final Resource target) {
		System.out.format("%s answers to %s%n", target.getName(), name);
	}

	public String getName() {
		return name;
	}
}
