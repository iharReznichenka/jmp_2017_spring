package com.epam.reznichenka.jmp.troubleshooting.task5;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


/**
 * Implement java (Java 1.6) application that reads the attached text data file line by line.
 * The application should take 3 first characters (using substring() method) of every line and put all the 3-character lines to
 * ArrayList.
 */
public class Task5AppWithoutLeak
{
	public void run()
	{
		CommonFunctionality commonFunctionality = new CommonFunctionality();
		try
		{
			ArrayList<String> words = commonFunctionality.readTestData("task5_data.txt");
			words.trimToSize();
			List<List<String>> threeWordLists = representAsThreeWordLists(words);
			words = null;
			System.out.println("Starting infinity loop to be able to make memory dump");
			while (true)
			{
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	private List<List<String>> representAsThreeWordLists(List<String> words)
	{
		List<List<String>> threeWordLists = new ArrayList<>();

		while (words.size() >= 3)
		{
			int toEndOfListIndex = words.size() - 1;
			int lastThreeWordsIndex = toEndOfListIndex - 2;
			threeWordLists.add(words.subList(lastThreeWordsIndex, toEndOfListIndex));
			removeLastThreeElements(words);
		}

		return threeWordLists;
	}

	private void removeLastThreeElements(List<String> words)
	{
		ListIterator<String> stringListIterator = words.listIterator(words.size() - 1);
		for (int count = 0; count < 3; count++)
		{
			if (stringListIterator.hasPrevious())
			{
				stringListIterator.previous();
				stringListIterator.remove();
			}
		}
	}
}
