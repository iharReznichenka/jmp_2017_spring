package com.epam.reznichenka.jmp.troubleshooting.task3;

import com.epam.reznichenka.jmp.troubleshooting.model.Resource;
import com.epam.reznichenka.jmp.troubleshooting.runnable.ConnectResources;

/**
 * Deadlock with 4 shared resources
 */
public class Task3App {

	public void run() {
		Resource first = new Resource("First");
		Resource second = new Resource("Second");
		Resource third = new Resource("Third");
		Resource fourth = new Resource("Fourth");

		ConnectResources firstToSecond = new ConnectResources(first, second);
		ConnectResources secondToThird = new ConnectResources(second, third);
		ConnectResources thirdToFourth = new ConnectResources(third, fourth);
		ConnectResources fourthToFirst = new ConnectResources(fourth, first);
		new Thread(firstToSecond).start();
		new Thread(secondToThird).start();
		new Thread(thirdToFourth).start();
		new Thread(fourthToFirst).start();
	}

}
