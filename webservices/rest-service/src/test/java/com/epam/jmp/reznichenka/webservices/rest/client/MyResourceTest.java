package com.epam.jmp.reznichenka.webservices.rest.client;

import com.epam.jmp.reznichenka.webservices.rest.resource.MyResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyResourceTest extends JerseyTest {

	private static final String RESOURCE = "myresource";

	@Override
	protected ResourceConfig configure() {
		enable(TestProperties.LOG_TRAFFIC);
		return new ResourceConfig(MyResource.class);
	}

	@Test
	public void getsIt() {
		String responseMessage = target().path(RESOURCE).request().get(String.class);
		assertEquals("Got it!", responseMessage);
	}
}
