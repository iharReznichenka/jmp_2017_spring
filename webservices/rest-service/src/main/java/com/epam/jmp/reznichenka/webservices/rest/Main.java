package com.epam.jmp.reznichenka.webservices.rest;

import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.ext.RuntimeDelegate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Entry point
 */
public class Main {

	public static final String ADDRESS = "localhost";
	public static final int PORT = 9090;
	public static final String WEB_ROOT = "/webroot/";
	public static final String APP_PATH = "/app/";

	public static HttpServer startServer() {
		final HttpServer server = new HttpServer();
		Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));
		final NetworkListener listener = new NetworkListener("grizzly", ADDRESS, PORT);
		server.addListener(listener);

		final ServerConfiguration config = server.getServerConfiguration();
		CLStaticHttpHandler staticContentHandler = new CLStaticHttpHandler(Main.class.getClassLoader(), WEB_ROOT);
		config.addHttpHandler(staticContentHandler, APP_PATH);

		GrizzlyHttpContainer restEndpointHandler = RuntimeDelegate.getInstance().createEndpoint(new AppResourceConfig(), GrizzlyHttpContainer.class);
		config.addHttpHandler(restEndpointHandler, APP_PATH);

		try {
			server.start();
		} catch (Exception ex) {
			throw new ProcessingException("Exception thrown when trying to start grizzly server", ex);
		}

		return server;
	}

	public static void main(final String[] args) {

		try {
			startServer();
			System.out.println(String.format("Application started.\n"
								 + "Access it at %s\n"
								 + "Stop the application using CTRL+C",
					  getAppUri()));

			Thread.currentThread().join();
		} catch (InterruptedException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static String getAppUri() {
		return String.format("http://%s:%s%s", ADDRESS, PORT, APP_PATH);
	}
}

