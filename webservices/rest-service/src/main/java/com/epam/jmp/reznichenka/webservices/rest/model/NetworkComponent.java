package com.epam.jmp.reznichenka.webservices.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class NetworkComponent {

	private Long id;
	private String description;

	public abstract int getWorkload();

	public abstract void doOperation(final Runnable runnable);

	public Long getId() {
		return id;
	}

	public NetworkComponent setId(Long id) {
		this.id = id;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public NetworkComponent setDescription(String description) {
		this.description = description;
		return this;
	}
}
