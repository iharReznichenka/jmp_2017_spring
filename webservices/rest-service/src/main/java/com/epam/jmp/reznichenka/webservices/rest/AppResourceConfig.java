package com.epam.jmp.reznichenka.webservices.rest;

import org.glassfish.jersey.server.ResourceConfig;

public class AppResourceConfig extends ResourceConfig {

	public AppResourceConfig() {
		packages("com.epam.jmp.reznichenka.webservices.rest.resource");
	}
}
