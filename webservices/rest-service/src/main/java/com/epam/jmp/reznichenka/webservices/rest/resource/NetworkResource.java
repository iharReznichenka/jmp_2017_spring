package com.epam.jmp.reznichenka.webservices.rest.resource;

import com.epam.jmp.reznichenka.webservices.rest.model.Network;
import com.epam.jmp.reznichenka.webservices.rest.repository.NetworkRepository;
import com.epam.jmp.reznichenka.webservices.rest.repository.impl.MockNetworkRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("networks")
@Produces({MediaType.APPLICATION_JSON})
public class NetworkResource {

	private NetworkRepository repository = new MockNetworkRepository();

	@GET
	public Response all() {
		Collection<Network> result = repository.findAll();
		if (result == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok()
				  .entity(new GenericEntity<Collection<Network>>(result) {
				  })
				  .build();
	}

}
