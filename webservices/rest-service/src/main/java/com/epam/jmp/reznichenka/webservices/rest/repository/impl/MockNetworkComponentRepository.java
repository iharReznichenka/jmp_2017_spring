package com.epam.jmp.reznichenka.webservices.rest.repository.impl;

import com.epam.jmp.reznichenka.webservices.rest.model.ClusterNetworkComponent;
import com.epam.jmp.reznichenka.webservices.rest.model.NetworkComponent;
import com.epam.jmp.reznichenka.webservices.rest.model.SingleNetworkComponent;
import com.epam.jmp.reznichenka.webservices.rest.repository.NetworkComponentRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Mocked implementation of {@link NetworkComponentRepository}
 */
public class MockNetworkComponentRepository implements NetworkComponentRepository {

	private static final AtomicLong sequence = new AtomicLong();
	private static final Collection<NetworkComponent> components = new ArrayList<>();

	static {
		components.add(new SingleNetworkComponent()
				  .setId(sequence.incrementAndGet())
				  .setDescription("single"));

		NetworkComponent cluster = new ClusterNetworkComponent()
				  .setComponents(Collections.singletonList(new SingleNetworkComponent()
							 .setId(sequence.incrementAndGet())
							 .setDescription("single inside cluster")))
				  .setId(sequence.incrementAndGet())
				  .setDescription("cluster");
		components.add(cluster);
	}

	@Override
	public Collection<NetworkComponent> findAll() {
		return components;
	}
}
