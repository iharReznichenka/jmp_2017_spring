package com.epam.jmp.reznichenka.webservices.rest.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.concurrent.atomic.AtomicInteger;

@XmlRootElement
public class SingleNetworkComponent extends NetworkComponent {

	private AtomicInteger workload = new AtomicInteger();

	@Override
	public int getWorkload() {
		return workload.get();
	}

	@Override
	public void doOperation(Runnable runnable) {
		workload.incrementAndGet();
		runnable.run();
	}
}
