package com.epam.jmp.reznichenka.webservices.rest.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement
public class Network {
	private Long id;
	private Collection<NetworkComponent> components;

	public Network(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Collection<NetworkComponent> getComponents() {
		return components;
	}

	public Network setComponents(final Collection<NetworkComponent> components) {
		this.components = components;
		return this;
	}
}
