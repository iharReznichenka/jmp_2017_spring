package com.epam.jmp.reznichenka.webservices.rest.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

@XmlRootElement
public class ClusterNetworkComponent extends NetworkComponent {

	private Collection<NetworkComponent> components = new ArrayList<>();

	@Override
	public int getWorkload() {
		return components.stream().mapToInt(NetworkComponent::getWorkload).sum();
	}

	@Override
	public void doOperation(final Runnable operation) {
		NetworkComponent theMostLoadedComponent = components.stream().sorted(Comparator.comparingInt(NetworkComponent::getWorkload))
				  .findFirst()
				  .orElseThrow(() -> new RuntimeException("Customer has no elements"));
		theMostLoadedComponent.doOperation(operation);
	}

	public void addComponent(NetworkComponent component) {
		components.add(component);
	}

	public ClusterNetworkComponent setComponents(Collection<NetworkComponent> components) {
		this.components = components;
		return this;
	}

	public Collection<NetworkComponent> getComponents() {
		return components;
	}
}
