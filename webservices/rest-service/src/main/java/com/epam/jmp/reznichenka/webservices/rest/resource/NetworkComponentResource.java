package com.epam.jmp.reznichenka.webservices.rest.resource;

import com.epam.jmp.reznichenka.webservices.rest.model.NetworkComponent;
import com.epam.jmp.reznichenka.webservices.rest.repository.NetworkComponentRepository;
import com.epam.jmp.reznichenka.webservices.rest.repository.impl.MockNetworkComponentRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("components")
@Produces({MediaType.APPLICATION_JSON})
public class NetworkComponentResource {

	private NetworkComponentRepository repository = new MockNetworkComponentRepository();

	@GET
	public Response getAll() {
		Collection<NetworkComponent> components = repository.findAll();
		return Response.ok()
				  .entity(new GenericEntity<Collection<NetworkComponent>>(components) {
				  })
				  .build();
	}
}
