package com.epam.jmp.reznichenka.webservices.rest.repository;

import com.epam.jmp.reznichenka.webservices.rest.model.NetworkComponent;

import java.util.Collection;

public interface NetworkComponentRepository {

	Collection<NetworkComponent> findAll();

}
