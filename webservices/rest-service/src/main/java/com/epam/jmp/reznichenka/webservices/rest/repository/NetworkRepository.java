package com.epam.jmp.reznichenka.webservices.rest.repository;

import com.epam.jmp.reznichenka.webservices.rest.model.Network;

import java.util.Collection;

/**
 * DAO layer for {@link Network} entity
 */
public interface NetworkRepository {
	Collection<Network> findAll();
}
