package com.epam.jmp.reznichenka.webservices.rest.repository.impl;

import com.epam.jmp.reznichenka.webservices.rest.model.Network;
import com.epam.jmp.reznichenka.webservices.rest.model.NetworkComponent;
import com.epam.jmp.reznichenka.webservices.rest.model.SingleNetworkComponent;
import com.epam.jmp.reznichenka.webservices.rest.repository.NetworkRepository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Mocked in-memory {@link NetworkRepository} implementation
 */
public class MockNetworkRepository implements NetworkRepository {

	private static final Map<Long, Network> networks = new HashMap<>();
	private static final AtomicLong sequence = new AtomicLong();

	static {
		long id = sequence.incrementAndGet();
		List<NetworkComponent> components = Collections.singletonList(new SingleNetworkComponent().setId(id).setDescription("sample"));
		networks.put(id, new Network(id).setComponents(components));
	}

	@Override
	public Collection<Network> findAll() {
		return networks.values();
	}

}
