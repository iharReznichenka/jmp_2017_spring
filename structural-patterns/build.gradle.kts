allprojects {
    group = "com.epam.jmp.reznichenka"
    version = "0.1"
}

plugins {
    base
    java
}

dependencies {
    // Make the root project archives configuration depend on every subproject
    subprojects.forEach {
        archives(it)
    }
}
