package com.epam.jmp.reznichenka.creationalpatterns.service

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.ComputerNotReadyForSetupException
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.*
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.ComputerPartsService
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl.CheapComputerPartsService
import spock.lang.Specification

/**
 * Specifies behavior for computer parts service interface
 */
class ComputerPartsServiceSpec extends Specification {

    ComputerPartsService target = new CheapComputerPartsService()

    def "when computers has no system block than adds a new one"() {
        given:
        def computer = Mock(Computer) {
            getSystemBlock() >> null
        }
        when:
        target.setupSystemBlock(computer)
        then:
        1 * computer.setSystemBlock(_ as SystemBlock)
    }

    def "when computer already has system block then do nothing"() {
        given:
        def computer = Mock(Computer) {
            getSystemBlock() >> Mock(SystemBlock)
        }
        when:
        target.setupSystemBlock(computer)
        then:
        0 * computer.setSystemBlock(_ as SystemBlock)
    }

    def "when computer has no mouse then setups a new one"() {
        given:
        def computer = Mock(Computer) {
            getMouse() >> null
        }
        when:
        target.setupMouse(computer)
        then:
        1 * computer.setMouse(_ as Mouse)
    }

    def "when computer already has a mouse then do nothing"() {
        given:
        def computer = Mock(Computer) {
            getMouse() >> Mock(Mouse)
        }
        when:
        target.setupMouse(computer)
        then:
        0 * computer.setMouse(_ as Mouse)
    }

    def "when computer has no keyboard then add a new one"() {
        given: "computer without keyboard"
        def computer = Mock(Computer) {
            getKeyboard() >> Mock(Keyboard)
        }
        when: "keyboard is setting up"
        target.setupKeyboard(computer)
        then: "adds a new one"
        1 * computer.setKeyboard(_ as Keyboard)
    }

    def "when computer already has a keyboard then do nothing"() {
        given:
        def computer = Mock(Computer) {
            getKeyboard() >> Mock(Keyboard)
        }
        when:
        target.setupKeyboard(computer)
        then:
        0 * computer.setKeyboard(_ as Keyboard)
    }

    def "when computer has no monitor than adds a new one"() {
        given:
        def computer = Mock(Computer) {
            getMonitor() >> null
        }
        when:
        target.setupMonitor(computer)
        then:
        1 * computer.setMonitor(_ as Monitor)
    }

    def "when computer already has monitor then do nothing"() {
        given:
        def computer = Mock(Computer) {
            getMonitor() >> Mock(Monitor)
        }
        when:
        target.setupMonitor(computer)
        then:
        0 * computer.setMonitor(_ as Monitor)
    }

    def "when computer misses some of components then os can't be setup"() {
        given:
        def computer = newComputer(source)
        when:
        target.setupOs(computer)
        then:
        def ex = thrown(ComputerNotReadyForSetupException)
        ex.message == expected
        where:
        source                                                                                          | expected
        [monitor: null, keyboard: Mock(Keyboard), mouse: Mock(Mouse), systemBlock: Mock(SystemBlock)]   | ComputerPartsService.MONITOR_IS_MISSING
        [monitor: Mock(Monitor), keyboard: Mock(Keyboard), mouse: null, systemBlock: Mock(SystemBlock)] | ComputerPartsService.MOUSE_IS_MISSING
        [monitor: Mock(Monitor), keyboard: Mock(Keyboard), mouse: Mock(Mouse), systemBlock: null]       | ComputerPartsService.SYSTEM_BLOCK_IS_MISSING
        [monitor: Mock(Monitor), keyboard: null, mouse: Mock(Mouse), systemBlock: Mock(SystemBlock)]    | ComputerPartsService.KEYBOARD_IS_MISSING
    }

    Computer newComputer(def opts) {
        return new Computer().with {
            monitor = opts.monitor ?: null
            keyboard = opts.keyboard ?: null
            mouse = opts.mouse ?: null
            systemBlock = opts.systemBlock ?: null
            it
        }
    }

}