package com.epam.jmp.reznichenka.creationalpatterns.input.impl

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.EmptyUserInputException
import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.UnknownComputerTypeException
import com.epam.jmp.reznichenka.creationalpatterns.factory.input.UserInputParser
import com.epam.jmp.reznichenka.creationalpatterns.factory.input.impl.DefaultUserInputParser
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.ComputerType
import spock.lang.Specification

class DefaultUserInputParserSpec extends Specification {

    def "validates application args during object instantiation"() {
        given:
        def args = source
        when:
        new DefaultUserInputParser(args)
        then:
        def ex = thrown(EmptyUserInputException)
        ex.message == expected
        where:
        source         | expected
        [] as String[] | UserInputParser.ARGUMENTS_SHOULD_NOT_BE_EMPTY
        null           | UserInputParser.ARGUMENTS_SHOULD_NOT_BE_EMPTY
    }

    def "when args is not empty then correctly instantiates object"() {
        given:
        def args = ["someComputerType"] as String[]
        when:
        new DefaultUserInputParser(args)
        then:
        noExceptionThrown()
    }

    def "can define computer type from user input"() {
        given:
        def args = source
        def target = new DefaultUserInputParser(args)
        when:
        def result = target.getComputerType()
        then:
        noExceptionThrown()
        result == expected
        where:
        source                             | expected
        ["cheap"] as String[]              | ComputerType.CHEAP
        ["cHeaP"] as String[]              | ComputerType.CHEAP
        ["expensive"] as String[]          | ComputerType.EXPENSIVE
        ["exPENsive"] as String[]          | ComputerType.EXPENSIVE
        ["cheap", "expensive"] as String[] | ComputerType.CHEAP
    }

    def "throws exception when computer type can't be resolved"() {
        given:
        def computerTypeString = "magic computer"
        def args = [computerTypeString] as String[]
        def target = new DefaultUserInputParser(args)
        when:
        target.getComputerType()
        then:
        def ex = thrown(UnknownComputerTypeException)
        ex.message == String.format(UnknownComputerTypeException.UNKNOWN_COMPUTER_TYPE, computerTypeString)
    }
}