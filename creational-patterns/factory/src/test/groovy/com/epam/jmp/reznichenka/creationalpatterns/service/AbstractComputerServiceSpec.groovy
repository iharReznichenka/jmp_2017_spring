package com.epam.jmp.reznichenka.creationalpatterns.service

import com.epam.jmp.reznichenka.creationalpatterns.factory.model.ComputerType
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl.CheapComputerService
import spock.lang.Specification

/**
 * Specifies behavior both for method factory and abstract factory implementations
 */
class AbstractComputerServiceSpec extends Specification {

    def target = new CheapComputerService()

    def "when type is defined then instance of computer is created"() {
        when:
        def result = target.process(source)
        then:
        result != expected
        where:
        source                 | expected
        ComputerType.CHEAP     | null
        ComputerType.EXPENSIVE | null
    }

    def "computer contains all required components"() {
        given: "type is defined"
        def type = ComputerType.CHEAP
        when:
        def computer = target.process(type)
        then:
        computer.systemBlock != null
        computer.monitor != null
        computer.keyboard != null
        computer.mouse != null
        computer.os != null
    }
}