package com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl;

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.ComputerNotReadyForSetupException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.ComputerPartsService;

import java.util.Objects;

public abstract class AbstractComputerPartsService implements ComputerPartsService {

	protected static final String OS_SETUP_FAILED = "OS setup failed for computer {}";

	protected void isValidateForOsSetup(final Computer computer) {
		if (Objects.isNull(computer.getMonitor())) {
			throw new ComputerNotReadyForSetupException(MONITOR_IS_MISSING);
		} else if (Objects.isNull(computer.getMouse())) {
			throw new ComputerNotReadyForSetupException(MOUSE_IS_MISSING);
		} else if (Objects.isNull(computer.getKeyboard())) {
			throw new ComputerNotReadyForSetupException(KEYBOARD_IS_MISSING);
		} else if (Objects.isNull(computer.getSystemBlock())) {
			throw new ComputerNotReadyForSetupException(SYSTEM_BLOCK_IS_MISSING);
		}
	}
}
