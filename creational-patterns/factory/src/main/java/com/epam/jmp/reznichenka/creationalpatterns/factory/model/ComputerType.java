package com.epam.jmp.reznichenka.creationalpatterns.factory.model;

/**
 * Defines available types of {@link Computer}
 */
public enum ComputerType {
	CHEAP, EXPENSIVE
}
