package com.epam.jmp.reznichenka.creationalpatterns.factory;


import com.epam.jmp.reznichenka.creationalpatterns.factory.factory.ComputerServiceFactory;
import com.epam.jmp.reznichenka.creationalpatterns.factory.input.UserInputParser;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.ComputerType;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.AbstractComputerService;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static final Logger log = LoggerFactory.getLogger(App.class);
	private UserInputParser inputParser;

	public App(@NotNull UserInputParser inputParser) {
		this.inputParser = inputParser;
	}

	public void run() {
		ComputerType computerType = inputParser.getComputerType();
		AbstractComputerService computerService = ComputerServiceFactory.getComputerService(computerType);
		Computer result = computerService.process();
		log.info("{}", result);
	}
}
