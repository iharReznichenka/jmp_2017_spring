package com.epam.jmp.reznichenka.creationalpatterns.factory.input.impl;

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.EmptyUserInputException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.UnknownComputerTypeException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.input.UserInputParser;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.ComputerType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;


/**
 * Default implementation of {@link UserInputParser}
 */
public class DefaultUserInputParser implements UserInputParser {
	private static final int COMPUTER_TYPE_INDEX = 0;

	@NotNull
	private final String[] args;
	@NotNull
	private final ComputerType computerType;

	public DefaultUserInputParser(final String... args) throws EmptyUserInputException, UnknownComputerTypeException {
		validate(args);
		this.args = args;
		this.computerType = defineComputerType();
	}

	private ComputerType defineComputerType() throws UnknownComputerTypeException {
		String typeFromInput = args[COMPUTER_TYPE_INDEX];
		return Arrays.stream(ComputerType.values())
				  .map(Enum::name)
				  .filter(it -> it.equalsIgnoreCase(typeFromInput))
				  .findFirst()
				  .map(ComputerType::valueOf)
				  .orElseThrow(() -> new UnknownComputerTypeException(typeFromInput));
	}

	@NotNull
	@Override
	public ComputerType getComputerType() {
		return computerType;
	}

	private void validate(@Nullable String... args) throws EmptyUserInputException {
		if (args == null || args.length == 0) {
			throw new EmptyUserInputException(ARGUMENTS_SHOULD_NOT_BE_EMPTY);
		}
	}
}
