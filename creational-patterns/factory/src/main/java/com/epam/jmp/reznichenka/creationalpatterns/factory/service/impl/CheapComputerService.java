package com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl;

import com.epam.jmp.reznichenka.creationalpatterns.factory.service.AbstractComputerService;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.ComputerPartsService;

/**
 * Method Factory pattern implementation of {@link AbstractComputerService}
 */
public class CheapComputerService extends AbstractComputerService {

	@Override
	protected ComputerPartsService getComputerPartsService() {
		return CheapComputerPartsService.getInstance();
	}

	private static final class SingletonHolder {
		private static final CheapComputerService INSTANCE = new CheapComputerService();

		private SingletonHolder() {
		}
	}

	public static CheapComputerService getInstance() {
		return SingletonHolder.INSTANCE;
	}
}
