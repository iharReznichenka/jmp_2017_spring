package com.epam.jmp.reznichenka.creationalpatterns.factory.exception;

/**
 * Should be used when user inputs incorrect data
 */
public class EmptyUserInputException extends Exception {

	public EmptyUserInputException(String message) {
		super(message);
	}

	public EmptyUserInputException(String message, Throwable cause) {
		super(message, cause);
	}
}
