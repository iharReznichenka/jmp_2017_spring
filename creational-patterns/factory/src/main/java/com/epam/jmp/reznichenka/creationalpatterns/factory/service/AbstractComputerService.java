package com.epam.jmp.reznichenka.creationalpatterns.factory.service;

import com.epam.jmp.reznichenka.creationalpatterns.factory.model.Computer;
import org.jetbrains.annotations.NotNull;

/**
 * Creates instances of {@link com.epam.jmp.reznichenka.creationalpatterns.factory.model.Computer}
 */
public abstract class AbstractComputerService {

	@NotNull
	public Computer process() {
		Computer computer = new Computer();
		getComputerPartsService().setupKeyboard(computer);
		getComputerPartsService().setupMonitor(computer);
		getComputerPartsService().setupMouse(computer);
		getComputerPartsService().setupSystemBlock(computer);
		return getComputerPartsService().setupOs(computer);
	}

	protected abstract ComputerPartsService getComputerPartsService();

}
