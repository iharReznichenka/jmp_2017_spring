package com.epam.jmp.reznichenka.creationalpatterns.factory.input;

import com.epam.jmp.reznichenka.creationalpatterns.factory.model.ComputerType;
import org.jetbrains.annotations.NotNull;

/**
 * User should be able to input the type of computer
 */
public interface UserInputParser {
	String ARGUMENTS_SHOULD_NOT_BE_EMPTY = "Arguments should not be empty";

	@NotNull
	ComputerType getComputerType();
}
