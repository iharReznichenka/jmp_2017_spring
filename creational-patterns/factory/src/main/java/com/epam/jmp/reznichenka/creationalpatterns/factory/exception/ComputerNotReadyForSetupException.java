package com.epam.jmp.reznichenka.creationalpatterns.factory.exception;

/**
 * Should be thrown when computer misses some required conditions
 */
public class ComputerNotReadyForSetupException extends RuntimeException {
	public ComputerNotReadyForSetupException(String message) {
		super(message);
	}
}
