package com.epam.jmp.reznichenka.creationalpatterns.factory;

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.EmptyUserInputException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.UnknownComputerTypeException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.input.UserInputParser;
import com.epam.jmp.reznichenka.creationalpatterns.factory.input.impl.DefaultUserInputParser;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entry point
 */
public class Main {

	private static final Logger log = LoggerFactory.getLogger(Main.class);

	private Main() {
	}

	public static void main(String... args) {
		try {
			@NotNull UserInputParser inputParser = new DefaultUserInputParser(args);
			new App(inputParser).run();
		} catch (EmptyUserInputException | UnknownComputerTypeException ex) {
			log.debug("Failed to setup application", ex);
		}
	}
}
