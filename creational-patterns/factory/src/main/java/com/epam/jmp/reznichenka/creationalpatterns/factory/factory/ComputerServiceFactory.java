package com.epam.jmp.reznichenka.creationalpatterns.factory.factory;

import com.epam.jmp.reznichenka.creationalpatterns.factory.model.ComputerType;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.AbstractComputerService;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl.CheapComputerService;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl.ExpensiveComputerService;

public class ComputerServiceFactory {

	private ComputerServiceFactory() {
	}

	public static AbstractComputerService getComputerService(ComputerType computerType) {
		if (ComputerType.EXPENSIVE.equals(computerType)) {
			return ExpensiveComputerService.getInstance();
		}
		return CheapComputerService.getInstance();
	}

}
