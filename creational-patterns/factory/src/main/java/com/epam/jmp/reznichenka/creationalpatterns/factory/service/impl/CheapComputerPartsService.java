package com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl;

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.ComputerNotReadyForSetupException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.cheap.*;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.ComputerPartsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Default implementation of {@link ComputerPartsService}
 */
public class CheapComputerPartsService extends AbstractComputerPartsService {

	private static final Logger log = LoggerFactory.getLogger(CheapComputerPartsService.class);

	@Override
	public Computer setupSystemBlock(final Computer computer) {
		if (Objects.nonNull(computer.getSystemBlock())) {
			return computer;
		}
		return computer.setSystemBlock(new CheapSystemBlock());
	}

	@Override
	public Computer setupMouse(final Computer computer) {
		if (Objects.nonNull(computer.getMouse())) {
			return computer;
		}
		return computer.setMouse(new CheapMouse());
	}

	@Override
	public Computer setupKeyboard(final Computer computer) {
		if (Objects.nonNull(computer.getKeyboard())) {
			return computer;
		}
		return computer.setKeyboard(new CheapKeyboard());
	}

	@Override
	public Computer setupMonitor(final Computer computer) {
		if (Objects.nonNull(computer.getMonitor())) {
			return computer;
		}
		return computer.setMonitor(new CheapMonitor());
	}

	@Override
	public Computer setupOs(final Computer computer) {
		try {
			isValidateForOsSetup(computer);
		} catch (ComputerNotReadyForSetupException ex) {
			log.info(OS_SETUP_FAILED, computer, ex);
		}
		return computer.setOs(new CheapOs());
	}

	private static class SingletonHolder {
		private static final CheapComputerPartsService INSTANCE = new CheapComputerPartsService();

		private SingletonHolder() {
		}
	}

	public static CheapComputerPartsService getInstance() {
		return SingletonHolder.INSTANCE;
	}
}
