package com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl;

import com.epam.jmp.reznichenka.creationalpatterns.factory.service.AbstractComputerService;
import com.epam.jmp.reznichenka.creationalpatterns.factory.service.ComputerPartsService;

public class ExpensiveComputerService extends AbstractComputerService {

	private ExpensiveComputerService() {

	}

	@Override
	protected ComputerPartsService getComputerPartsService() {
		return ExpensiveComputerPartsService.getInstance();
	}

	private static final class SingletonHolder {
		private static final ExpensiveComputerService INSTANCE = new ExpensiveComputerService();
	}

	public static ExpensiveComputerService getInstance() {
		return SingletonHolder.INSTANCE;
	}
}
