package com.epam.jmp.reznichenka.creationalpatterns.factory.service.impl;

import com.epam.jmp.reznichenka.creationalpatterns.factory.exception.ComputerNotReadyForSetupException;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.*;
import com.epam.jmp.reznichenka.creationalpatterns.factory.model.expensive.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Created by Ihar on 06.03.2017.
 */
public class ExpensiveComputerPartsService extends AbstractComputerPartsService {

	private static final Logger log = LoggerFactory.getLogger(ExpensiveComputerPartsService.class);

	@Override
	public Computer setupSystemBlock(final Computer computer) {
		if (Objects.nonNull(computer.getSystemBlock())) {
			return computer;
		}
		return computer.setSystemBlock(new ExpensiveSystemBlock());
	}

	@Override
	public Computer setupMouse(final Computer computer) {
		if (Objects.nonNull(computer.getMouse())) {
			return computer;
		}
		return computer.setMouse(new ExpensiveMouse());
	}

	@Override
	public Computer setupKeyboard(final Computer computer) {
		if (Objects.nonNull(computer.getKeyboard())) {
			return computer;
		}
		return computer.setKeyboard(new ExpensiveKeyboard());
	}

	@Override
	public Computer setupMonitor(final Computer computer) {
		if (Objects.nonNull(computer.getMonitor())) {
			return computer;
		}
		return computer.setMonitor(new ExpensiveMonitor());
	}

	@Override
	public Computer setupOs(final Computer computer) {
		try {
			isValidateForOsSetup(computer);
		} catch (ComputerNotReadyForSetupException ex) {
			log.info(OS_SETUP_FAILED, computer, ex);
			return computer;
		}
		return computer.setOs(new ExpensiveOs());
	}


	private static class SingletonHolder {
		private static final ExpensiveComputerPartsService INSTANCE = new ExpensiveComputerPartsService();

		private SingletonHolder() {
		}
	}

	public static ExpensiveComputerPartsService getInstance() {
		return ExpensiveComputerPartsService.SingletonHolder.INSTANCE;
	}
}
