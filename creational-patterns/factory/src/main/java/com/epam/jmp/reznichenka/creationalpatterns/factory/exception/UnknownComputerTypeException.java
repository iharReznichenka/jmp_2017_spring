package com.epam.jmp.reznichenka.creationalpatterns.factory.exception;

/**
 * Should be thrown when computer type can't be identified
 */
public class UnknownComputerTypeException extends Exception {

	public static final String UNKNOWN_COMPUTER_TYPE = "Unknown computer type of %s";

	public UnknownComputerTypeException(final String computerType) {
		super(String.format(UNKNOWN_COMPUTER_TYPE, computerType));
	}
}
