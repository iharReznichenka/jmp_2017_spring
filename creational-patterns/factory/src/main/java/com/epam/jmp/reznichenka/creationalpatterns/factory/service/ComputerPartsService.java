package com.epam.jmp.reznichenka.creationalpatterns.factory.service;

import com.epam.jmp.reznichenka.creationalpatterns.factory.model.Computer;

/**
 * Service to install computer parts to the concrete computer instance
 */
public interface ComputerPartsService {

	String MONITOR_IS_MISSING = "Monitor is missing";
	String MOUSE_IS_MISSING = "Mouse is missing";
	String KEYBOARD_IS_MISSING = "Keyboard is missing";
	String SYSTEM_BLOCK_IS_MISSING = "System block is missing";

	Computer setupSystemBlock(Computer computer);

	Computer setupMouse(Computer computer);

	Computer setupKeyboard(Computer computer);

	Computer setupMonitor(Computer computer);

	Computer setupOs(Computer computer);
}
