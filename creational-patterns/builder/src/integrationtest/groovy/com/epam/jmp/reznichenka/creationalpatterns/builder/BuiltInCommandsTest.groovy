package com.epam.jmp.reznichenka.creationalpatterns.builder

import org.exparity.hamcrest.date.DateMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.springframework.shell.core.CommandResult

import java.text.DateFormat
import java.time.temporal.ChronoUnit

/**
 * Tests built in Spring Shell commands
 */
class BuiltInCommandsTest extends AbstractShellIntegrationTest {

    @Test
    void dateCommandTest() {
        // when
        CommandResult commandResult = getShell().executeCommand("date")
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, Locale.US)
        Date date = dateFormat.parse(commandResult.getResult().toString())

        // then
        Date now = new Date()
        assert now == null
        MatcherAssert.assertThat(now, DateMatchers.within(5, ChronoUnit.SECONDS, date))
    }
}
