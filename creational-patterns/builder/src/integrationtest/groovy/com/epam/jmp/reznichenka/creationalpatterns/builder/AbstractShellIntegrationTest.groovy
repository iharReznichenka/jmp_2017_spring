package com.epam.jmp.reznichenka.creationalpatterns.builder

import org.junit.AfterClass
import org.junit.BeforeClass
import org.springframework.shell.Bootstrap
import org.springframework.shell.core.JLineShellComponent

/**
 * Test abstraction to help to test Spring Shell functionality
 */
abstract class AbstractShellIntegrationTest {

    private static JLineShellComponent shell

    @BeforeClass
    static void startUp() throws InterruptedException {
        Bootstrap bootstrap = new Bootstrap()
        shell = bootstrap.getJLineShellComponent()
    }

    @AfterClass
    static void shutdown() {
        shell.stop()
    }

    static JLineShellComponent getShell() {
        return shell
    }
}
