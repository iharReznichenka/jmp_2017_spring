package com.epam.jmp.reznichenka.creationalpatterns.builder.builder;

import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Computer;

import java.util.Arrays;
import java.util.Objects;

/**
 * Builder {@link com.epam.jmp.reznichenka.creationalpatterns.builder.model.Computer}
 */
public abstract class AbstractComputerBuilder {

	protected static final String CANT_SETUP_OS = "Can't setup OS due to missing requirements";

	public abstract void addSystemBlock();

	public abstract void addMonitor();

	public abstract void addKeyboard();

	public abstract void addMouse();

	public abstract void addOs();

	protected abstract Computer getTarget();

	protected abstract void validate();

	public Computer build() {
		validate();
		return getTarget();
	}

	protected boolean isOsReadyForSetup() {
		return !hasMissingRequirements();
	}

	private boolean hasMissingRequirements() {
		return anyNull(getTarget().getKeyboard(), getTarget().getMouse(), getTarget().getMonitor(), getTarget().getSystemBlock());
	}

	private boolean anyNull(Object... objects) {
		return Arrays.stream(objects).anyMatch(Objects::isNull);
	}
}
