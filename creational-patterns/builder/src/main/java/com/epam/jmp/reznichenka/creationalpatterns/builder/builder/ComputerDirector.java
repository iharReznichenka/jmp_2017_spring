package com.epam.jmp.reznichenka.creationalpatterns.builder.builder;

import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.builder.model.ComputerType;

import java.util.List;

/**
 * Controls builder
 */
public class ComputerDirector {

	public Computer orderComputer(String name, ComputerType type, List<String> components) {
		AbstractComputerBuilder builder = getBuilderFor(name, type);
		if (components.contains("monitor")) {
			builder.addMonitor();
		}
		if (components.contains("keyboard")) {
			builder.addKeyboard();
		}
		if (components.contains("mouse")) {
			builder.addMouse();
		}
		if (components.contains("systemBlock")) {
			builder.addSystemBlock();
		}
		if (components.contains("os")) {
			builder.addOs();
		}
		return builder.build();

	}

	private AbstractComputerBuilder getBuilderFor(String computerName, ComputerType type) {
		switch (type) {
			case CHEAP:
				return new CheapComputerBuilder(computerName);
			case EXPENSIVE:
				return new ExpensiveComputerBuilder(computerName);
			default:
				throw new IllegalArgumentException("Unknown computer type");
		}
	}

	private static final class SingletonHolder {
		private static final ComputerDirector INSTANCE = new ComputerDirector();

		private SingletonHolder() {
		}
	}

	public static ComputerDirector getInstance() {
		return SingletonHolder.INSTANCE;
	}
}
