package com.epam.jmp.reznichenka.creationalpatterns.builder.model.expensive;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Monitor;

public class ExpensiveMonitor extends Monitor {
	@Override
	public String toString() {
		return "ExpensiveMonitor{}";
	}
}
