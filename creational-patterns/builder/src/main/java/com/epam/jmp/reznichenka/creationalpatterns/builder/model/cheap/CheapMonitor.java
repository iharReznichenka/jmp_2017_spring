package com.epam.jmp.reznichenka.creationalpatterns.builder.model.cheap;

import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Monitor;

public class CheapMonitor extends Monitor {
	@Override
	public String toString() {
		return "CheapMonitor{}";
	}
}
