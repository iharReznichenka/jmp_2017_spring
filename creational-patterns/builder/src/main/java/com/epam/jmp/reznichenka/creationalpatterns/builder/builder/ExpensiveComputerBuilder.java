package com.epam.jmp.reznichenka.creationalpatterns.builder.builder;

import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.builder.model.expensive.*;

public class ExpensiveComputerBuilder extends AbstractComputerBuilder {

	private Computer target;

	ExpensiveComputerBuilder(String computerName) {
		this.target = new Computer(computerName);
	}

	@Override
	public void addSystemBlock() {
		target.setSystemBlock(new ExpensiveSystemBlock());
	}

	@Override
	public void addMonitor() {
		target.setMonitor(new ExpensiveMonitor());
	}

	@Override
	public void addKeyboard() {
		target.setKeyboard(new ExpensiveKeyboard());
	}

	@Override
	public void addMouse() {
		target.setMouse(new ExpensiveMouse());
	}

	@Override
	public void addOs() {
		if (isOsReadyForSetup()) {
			target.setOs(new ExpensiveOs());
		}
		throw new IllegalStateException(CANT_SETUP_OS);
	}

	@Override
	protected Computer getTarget() {
		return target;
	}

	@Override
	protected void validate() {
		// do nothing for now
	}
}
