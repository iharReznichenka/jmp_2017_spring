package com.epam.jmp.reznichenka.creationalpatterns.builder.model.cheap;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.SystemBlock;

public class CheapSystemBlock extends SystemBlock {
	@Override
	public String toString() {
		return "CheapSystemBlock{}";
	}
}
