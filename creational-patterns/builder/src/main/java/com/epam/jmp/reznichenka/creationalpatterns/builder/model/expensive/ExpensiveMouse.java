package com.epam.jmp.reznichenka.creationalpatterns.builder.model.expensive;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Mouse;

public class ExpensiveMouse extends Mouse {
	@Override
	public String toString() {
		return "ExpensiveMouse{}";
	}
}
