package com.epam.jmp.reznichenka.creationalpatterns.builder.model.expensive;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Keyboard;

public class ExpensiveKeyboard extends Keyboard {
	@Override
	public String toString() {
		return "ExpensiveKeyboard{}";
	}
}
