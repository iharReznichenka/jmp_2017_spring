package com.epam.jmp.reznichenka.creationalpatterns.builder;

import org.springframework.shell.Bootstrap;

import java.io.IOException;

/**
 * Entry point
 */
public class Main {
	private Main() {
	}

	/**
	 * Delegates to framework
	 */
	public static void main(String[] args) throws IOException {
		Bootstrap.main(args);
	}
}
