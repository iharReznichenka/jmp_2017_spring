package com.epam.jmp.reznichenka.creationalpatterns.builder.model.cheap;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Keyboard;

public class CheapKeyboard extends Keyboard {
	@Override
	public String toString() {
		return "CheapKeyboard{}";
	}
}
