package com.epam.jmp.reznichenka.creationalpatterns.builder.model.expensive;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.SystemBlock;

public class ExpensiveSystemBlock extends SystemBlock {
	@Override
	public String toString() {
		return "ExpensiveSystemBlock{}";
	}
}
