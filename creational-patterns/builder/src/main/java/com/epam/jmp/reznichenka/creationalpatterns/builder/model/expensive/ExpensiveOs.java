package com.epam.jmp.reznichenka.creationalpatterns.builder.model.expensive;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Os;

public class ExpensiveOs extends Os {
	@Override
	public String toString() {
		return "ExpensiveOs{}";
	}
}
