package com.epam.jmp.reznichenka.creationalpatterns.builder.model;

/**
 * Domain representation of Computer
 */
public class Computer {

	private String name;
	private Keyboard keyboard;
	private SystemBlock systemBlock;
	private Monitor monitor;
	private Mouse mouse;
	private Os os;

	public Computer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Keyboard getKeyboard() {
		return keyboard;
	}

	public Computer setKeyboard(Keyboard keyboard) {
		this.keyboard = keyboard;
		return this;
	}

	public SystemBlock getSystemBlock() {
		return systemBlock;
	}

	public Computer setSystemBlock(SystemBlock systemBlock) {
		this.systemBlock = systemBlock;
		return this;
	}

	public Monitor getMonitor() {
		return monitor;
	}

	public Computer setMonitor(Monitor monitor) {
		this.monitor = monitor;
		return this;
	}

	public Mouse getMouse() {
		return mouse;
	}

	public Computer setMouse(Mouse mouse) {
		this.mouse = mouse;
		return this;
	}

	public Os getOs() {
		return os;
	}

	public Computer setOs(Os os) {
		this.os = os;
		return this;
	}

	@Override
	public String toString() {
		return "Computer{" +
				  "name='" + name + '\'' +
				  ", keyboard=" + keyboard +
				  ", systemBlock=" + systemBlock +
				  ", monitor=" + monitor +
				  ", mouse=" + mouse +
				  ", os=" + os +
				  '}';
	}
}
