package com.epam.jmp.reznichenka.creationalpatterns.builder.commands;

import com.epam.jmp.reznichenka.creationalpatterns.builder.builder.ComputerDirector;
import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.builder.model.ComputerType;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Main set of commands
 */
@Component
public class ComputerBuilderCommands implements CommandMarker {

	public static final String COMPONENT_NAMES_DELIMITER = ",";

	@CliAvailabilityIndicator({"cb new"})
	public boolean isNewAvailable() {
		return true;
	}

	@CliCommand(value = "cb order", help = "Order new computer")
	public String doOrder(
			  @CliOption(key = {"name"}, mandatory = true, help = "Name of the computer")   final String name,
			  @CliOption(key = {"type"}, mandatory = true, help = "Specifies price type of computer") final ComputerType type,
			  @CliOption(key = {"components"}, mandatory = true, help = "Specifies the list of components to add. Delimiter is ';'", optionContext = "splittingRegex=;") final String[] components) {

		Computer computer = ComputerDirector.getInstance().orderComputer(name, type, Arrays.asList(components));

		return "You've successfully ordered " + type + " computer = " + computer;
	}
}
