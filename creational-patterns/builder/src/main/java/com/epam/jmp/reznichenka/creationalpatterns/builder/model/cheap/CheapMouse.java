package com.epam.jmp.reznichenka.creationalpatterns.builder.model.cheap;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Mouse;

public class CheapMouse extends Mouse {
	@Override
	public String toString() {
		return "CheapMouse{}";
	}
}