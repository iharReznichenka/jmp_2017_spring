package com.epam.jmp.reznichenka.creationalpatterns.builder.model;

/**
 * Defines available types of {@link Computer}
 */
public enum ComputerType {
	CHEAP, EXPENSIVE
}
