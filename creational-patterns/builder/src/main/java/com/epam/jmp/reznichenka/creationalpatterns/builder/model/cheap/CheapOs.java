package com.epam.jmp.reznichenka.creationalpatterns.builder.model.cheap;


import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Os;

public class CheapOs extends Os {
	@Override
	public String toString() {
		return "CheapOs{}";
	}
}
