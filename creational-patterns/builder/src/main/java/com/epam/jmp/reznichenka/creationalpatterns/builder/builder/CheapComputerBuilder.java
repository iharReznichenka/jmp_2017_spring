package com.epam.jmp.reznichenka.creationalpatterns.builder.builder;

import com.epam.jmp.reznichenka.creationalpatterns.builder.model.Computer;
import com.epam.jmp.reznichenka.creationalpatterns.builder.model.cheap.*;

public class CheapComputerBuilder extends AbstractComputerBuilder {

	private Computer target;

	CheapComputerBuilder(final String computerName) {
		this.target = new Computer(computerName);
	}

	@Override
	public void addSystemBlock() {
		target.setSystemBlock(new CheapSystemBlock());
	}

	@Override
	public void addMonitor() {
		target.setMonitor(new CheapMonitor());
	}

	@Override
	public void addKeyboard() {
		target.setKeyboard(new CheapKeyboard());
	}

	@Override
	public void addMouse() {
		target.setMouse(new CheapMouse());
	}

	@Override
	public void addOs() {
		if (isOsReadyForSetup()) {
			target.setOs(new CheapOs());
		}
		throw new IllegalStateException(CANT_SETUP_OS);
	}

	@Override
	protected Computer getTarget() {
		return target;
	}

	@Override
	protected void validate() {
		// do nothing for now
	}
}
