package com.epam.jmp.reznichenka.creationalpatterns.builder

import spock.lang.Specification


/**
 * Checks integrity with spock framework
 */
class HelloSpockSpec extends Specification {
    def "length of Spock's and his friends' names"() {
        expect:
        name.size() == length

        where:
        name     | length
        "Spock"  | 5
        "Kirk"   | 4
        "Scotty" | 6
    }
}