import org.gradle.api.JavaVersion.VERSION_1_8

plugins {
    application
    groovy
}

application {
    mainClassName = "com.epam.jmp.reznichenka.creationalpatterns.builder.Main"
}

java {
    sourceCompatibility = VERSION_1_8
    targetCompatibility = VERSION_1_8
}

repositories {
    mavenCentral()
    maven { setUrl("http://oss.sonatype.org/content/repositories/snapshots/") } // spock
    maven { setUrl("https://repo.spring.io/libs-milestone") } // spring shell
}

dependencies {
    runtime("log4j:log4j:1.2.17")
    compile("org.springframework.shell:spring-shell:1.2.0.M1")
    compile("org.codehaus.groovy:groovy-all:2.4.9")
    testCompile("org.spockframework:spock-core:1.1-groovy-2.4-rc-3")
    testCompile("org.hamcrest:hamcrest-all:1.3")
    testCompile("org.exparity:hamcrest-date:2.0.4")
}

defaultTasks("installDist")
