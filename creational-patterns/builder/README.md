**In order to execute shell application:**
1) install app using gradlew installDist
2) run application from `<app folder>/build/install/builder/bin/builder`
3) execute cb order command, use tab for autocompletion

Example of complete command is: `cb order --name super_computer --type CHEAP --components keyboard;mouse;systemBlock`